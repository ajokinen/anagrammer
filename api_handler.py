import json
import urllib2

from anagrammer import Anagrammer


class ApiHandler:
    def __init__(self):
        self._anagrammer = Anagrammer()

    def handle(self, event, context):
        """ Handle usage with REST API """

        str1, str2 = self._extract_strings(event)
        return self._build_response(str1, str2, self._anagrammer.is_proper_anagram(str1, str2))

    @staticmethod
    def _extract_strings(event):
        return urllib2.unquote(event['pathParameters']['str1']).decode('utf8'), urllib2.unquote(event['pathParameters']['str2']).decode('utf8')

    @staticmethod
    def _build_response(str1, str2, is_proper_anagram):
        body = {'str1': str1, 'str2': str2, 'isProperAnagram': str(is_proper_anagram)}
        response = {'isBase64Encoded': 'false', 'statusCode': 200, 'headers': {'Content-Type': 'application/json'},
                    'body': json.dumps(body)}
        return response
