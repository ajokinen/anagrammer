import unittest

from anagrammer import Anagrammer


class AnagrammerTestCase(unittest.TestCase):
    def setUp(self):
        self._anagrammer = Anagrammer()

    def test_simple_valid_anagram(self):
        self.assertTrue(self._anagrammer.is_proper_anagram('foo', 'ofo'))

    def test_simple_invalid_anagram(self):
        self.assertFalse(self._anagrammer.is_proper_anagram('foo', 'bar'))

    def test_valid_anagram_with_non_meaningful_chars(self):
        self.assertTrue(self._anagrammer.is_proper_anagram('Jukka Virtanen', 'J.A. Terva-Ninkku'))

    def test_invalid_anagram_with_non_meaningful_chars(self):
        self.assertFalse(self._anagrammer.is_proper_anagram('Jukka Virtanen', 'J.A. Terva-Kinkku'))


if __name__ == '__main__':
    unittest.main()
