import logging
import re
import sys


class Anagrammer():
    def __init__(self):
        pass

    def is_proper_anagram(self, str1, str2):
        s1, s2 = self._normalize_strings(str1, str2)
        return s1 == s2

    def _normalize_strings(self, str1, str2):
        s1, s2 = self._remove_non_meaningful_chars(str1, str2)
        logging.debug(str1 + ' -> ' + s1 + ' -> ' + s1.lower() + ' -> ' + str(sorted(s1.lower())))
        logging.debug(str2 + ' -> ' + s2 + ' -> ' + s2.lower() + ' -> ' + str(sorted(s2.lower())))

        return str(sorted(s1.lower())), str(sorted(s2.lower()))

    @staticmethod
    def _remove_non_meaningful_chars(str1, str2):
        chars_to_remove = ['  ', '-', '.']
        rx = '[' + re.escape(''.join(chars_to_remove)) + ']'
        s1 = re.sub(rx, '', str1)
        s2 = re.sub(rx, '', str2)
        return s1, s2


class _CliHandler:
    def __init__(self):
        pass

    def handle(self, cliargs):
        """ Handle usage with CLI """

        if len(cliargs) != 3:
            print "Usage: python anagrammer.py <str1> <str2>"
        else:
            str1, str2 = self._extract_strings_cli(cliargs)
            if Anagrammer().is_proper_anagram(str1, str2):
                print 'Hooray, \'{}\' is a proper anagram of \'{}\''.format(str1, str2)
            else:
                print 'Sorry, \'{}\' is NOT a proper anagram of \'{}\''.format(str1, str2)

    @staticmethod
    def _extract_strings_cli(cliargs):
        str1 = cliargs[1]
        str2 = cliargs[2]
        return str1, str2


if __name__ == '__main__':
    _CliHandler().handle(sys.argv)
